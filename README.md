Atixnet Tools 
=============

Description
-----------
Tools utilities for modules.

Introduction
------------
Drupal 8/9 module with helpers, lazy config forms and controller to help concentrate on development.

It provides abstraction classes:
* AtoolsConfigForm which help with setting up the configuration of a module
* AtoolsBlockBase which help with setting up the form of a block
* AtoolsController with the possibility to set up a controller that only returns a stripped response.

Its also provide a static class helper with some utility methods.

It allows the management and generation of configuration form from structured YML files.

Form generation
----------
The generation of the form is enabled by two files, which define the structure of the form and a list of fields in YML format.

These files must be placed in the ./config/tools/ folder of the module which uses the generation of settings.

An example of each of the files is provided in this module.

## Fields file.
The file %NAME%.fields.yml define a list of fields to be managed or saved by the form. It is a succession of field[: value] set in a YML way.

If you just need the fields list, because you override the getDefaultFieldValue() method in your form object with something more appropriate, you use the indexed array YAML format :

```yml
- debug
- cache
- cache_time
```

If you also want to specify the default values in the file, you have to use the associative array YAML format :

```yml
debug: false
cache: false
cache_time: 300
```

## Fichier de structure
The file %NAME%.form.yml define the structure to be used to generate the form.
It is more or less the structure you end up with when you declare your form array manually.

The only  are that you do not use the '#' in front of keys, and you declare children of an element in a '_fields' element.

Some particularities :
* Fields children of another are placed in a `_fields` element.
* Form properties name do not start by a hash `#`.
* It is possible to use containers (fieldsets, vertical_tabs, ...).
* All titles and descriptions are translate using Drupal t().

```yml 
general:
  type: fieldset
  title: General settings
  _fields:
    debug:
      type: checkbox
      title: 'Debug mode'
      description: 'Enable debug mode to show informations'
    cache:
      type: checkbox
      title: 'Activate cache'
      description: 'Enable caching'
    cache_time:
      type: textfield
      title: 'Cache expiration'
      description: 'Cache expiration in second. 0 or empty is for none (do not use)'
```

AtoolsConfigForm
------------

This class is typically to use with module settings form. It extends `ConfigFormBase` and allow you to simply declare the form depending on YML files in the `config/tools` directory of the module.

The two files will have to be named 'MODULE_NAME.settings.fields.yml' and 'MODULE_NAME.settings.form.yml'.

Here is an exemple of module settings :
```php
<?php

namespace Drupal\afreshdesk\Form;

use Drupal\atools\Form\AtoolsConfigForm;

/**
 * Configure freshdesk settings.
 */
class FreshdeskSettingsForm extends AtoolsConfigForm {

  /**
   * Module machine name.
   *
   * @var string
   */
  const MODULE_NAME = 'afreshdesk';

}
```

AtoolsController
------------

Extending this controller allow you to generate empty response pages, or with minimum markups elements for a special purpose, as to answer a web-hook or server to server communication.


Atools
------------

Helper class providing with utilities static methods as :
* getCache: Retrieve a client-side cache, only if module settings allow it (cache config field).
* setCache: Store a client-side cache, only if module settings allow it (cache config field).
* attacheJS: Keep trace of the element to attache as drupalSettings.
* getVarsArray: Return an array of key => value from a 'key::value' text, separate by return character.
* getConfVarsArray: Return an array of key => value from a given module setting.
* urlFromPath: Replace D7 url() function, return an URL from a path (internal or external).
* debug: Show debug messages if module enable it and user has permission. Can use Drupal Messenger or Watchdog.
* configGet: Get a module setting value from a key or the config object.
* validatePath: Helper function from the system_theme_settings form.
* watchdogException: Modified version of core watchdog_exception to allow full message on GuzzleHttp Exception.