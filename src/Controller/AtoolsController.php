<?php

namespace Drupal\atools\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AtoolsController.
 */
abstract class AtoolsController extends ControllerBase {

  /**
   * Markup parts.
   *
   * @var array
   */
  private $markups = [];

  /**
   * Add a new child to the markups array.
   *
   * @param string $text
   *   Text to add to a markup.
   * @param array $markup
   *   Optionnal element (markup or other) to
   *    be map to the new child.
   */
  protected function addMarkup(string $text, array $markup = []) {
    $markup += [
      '#type' => 'markup',
      '#markup' => $text,
      '#suffix' => '<br>',
    ];
    $this->markups[] = $markup;
  }

  /**
   * Clear the markups array.
   */
  protected function clearMarkups() {
    $this->markups = [];
  }

  /**
   * Get the markups array.
   */
  protected function getMarkups() {
    return $this->markups;
  }

  /**
   * Render the markups as an array to be passed to a response.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The rendered HTML.
   */
  protected function renderMarkups() {
    return \Drupal::service('renderer')->renderRoot($this->markups);
  }

  /**
   * Return a custom Response to display only the main component on a page.
   *
   * @param string $text
   *   (optional) Text to add to a markup.
   * @param array $build
   *   (optional) The render array to display.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Custom response.
   */
  protected function cleanResponse(string $text = '', array $build = []) {
    if ($text || !empty($build)) {
      $this->addMarkup($text, $build);
    }

    $response = new Response();
    $response->setContent($this->renderMarkups());

    return $response;
  }

}
