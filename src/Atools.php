<?php

namespace Drupal\atools;

use Drupal\Core\Render\Markup;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;
use Drupal\Component\Utility\UrlHelper;

/**
 * Helper class.
 */
class Atools {

  /**
   * Retrieve a client-side cache hash from cache.
   *
   * The hash cache is consulted more than once per request; we therefore cache
   * the results statically to avoid multiple database requests.
   *
   * This cache can be reset by CRON / ADMIN or can be FORCED on expiration !
   *
   * @param string $module
   *   The module machine_name that ask for cache.
   * @param string $cid
   *   The cache ID of the data to retrieve.
   * @param bool $force
   *   (Optional) Force the detection of expiration time
   *   and reset the cache if outdated. Defaults to FALSE.
   * @param bool $allow_invalid
   *   (Optional) If TRUE, a cache item may be returned even if it is expired or
   *   has been invalidated. Such items may sometimes be preferred, if the
   *   alternative is recalculating the value stored in the cache, especially
   *   if another concurrent request is already recalculating the same value.
   *   The "valid" property of the returned object indicates whether the item is
   *   valid or not. Defaults to FALSE.
   *
   * @return object|false
   *   The cache item or FALSE on failure.
   *
   * @see \Drupal\Core\Cache\CacheBackendInterface::getMultiple()
   */
  public static function getCache($module, $cid, $force = FALSE, $allow_invalid = FALSE) {
    $cache = &drupal_static(__FUNCTION__, []);

    if (!self::configGet($module, 'cache', FALSE)) {
      return FALSE;
    }
    if (!array_key_exists($cid, $cache)) {
      $cache[$cid] = \Drupal::cache()->get($cid, $allow_invalid);
      // Verify expiration time of cache and do not return it if expired !
      if ($cache[$cid] && $force && ($cache[$cid]->expire < REQUEST_TIME)) {
        return FALSE;
      }

      if ($cache[$cid] && isset($cache[$cid]->data)) {
        $cache[$cid] = $cache[$cid]->data;
      }
    }

    return $cache[$cid];
  }

  /**
   * Store a client-side cache hash in persistent cache.
   *
   * @param string $module
   *   The module machine_name that ask for cache.
   * @param string $cid
   *   The cache ID of the data to store.
   * @param mixed $data
   *   The data to store in the cache.
   *   Some storage engines only allow objects up to a maximum of 1MB in size to
   *   be stored by default. When caching large arrays or similar, take care to
   *   ensure $data does not exceed this size.
   * @param int $expire
   *   One of the following values:
   *   - CacheBackendInterface::CACHE_PERMANENT: Indicates that the item should
   *     not be removed unless it is deleted explicitly.
   *   - A Unix timestamp: Indicates that the item will be considered invalid
   *     after this time, i.e. it will not be returned by get() unless
   *     $allow_invalid has been set to TRUE. When the item has expired, it may
   *     be permanently deleted by the garbage collector at any time.
   * @param array $tags
   *   An array of tags to be stored with the cache item. These should normally
   *   identify objects used to build the cache item, which should trigger
   *   cache invalidation when updated. For example if a cached item represents
   *   a node, both the node ID and the author's user ID might be passed in as
   *   tags. For example ['node:123', 'node:456', 'user:789'].
   */
  public static function setCache($module, $cid, $data, $expire = Cache::PERMANENT, array $tags = []) {
    if (self::configGet($module, 'cache', FALSE)) {
      \Drupal::cache()->set($cid, $data, $expire, $tags);
    }
  }

  /**
   * Keep trace of the element to attache as drupalSettings.
   *
   * @param string $settings
   *   The main settings name.
   * @param string $key
   *   The setting key.
   * @param string $value
   *   The setting value.
   *
   * @staticvar array $elements
   *
   * @return array
   *   The settings array if no key and value are given.
   */
  public static function attacheJS($settings = NULL, $key = NULL, $value = NULL) {
    static $elements = [];

    if (!isset($elements[$settings])) {
      $elements[$settings] = [];
    }

    if (is_null($settings)) {
      $return = $elements;
      $elements = [];
      return $return;
    }
    elseif (is_null($key)) {
      return isset($elements[$settings]) ? $elements[$settings] : [];
    }
    elseif (is_null($value)) {
      return isset($elements[$settings][$key]) ? $elements[$settings][$key] : [];
    }
    else {
      $elements[$settings][$key] = $value;
    }
  }

  /**
   * Return an array of key => value.
   *
   * Explode the given string on return character ("\n").
   * Also delete the Windows "\r" character.
   *
   * @param string $string
   *   The string to split in array.
   *
   * @retur array
   *   An array of ID => Label.
   */
  public static function getVarsArray($string) {
    $vars = [];
    if (empty($string)) {
      return $vars;
    }
    $lines = explode("\n", str_replace("\r", '', $string));

    foreach ($lines as $line) {
      if (strstr($line, '::')) {
        list($id, $label) = explode('::', $line);
        $vars[$id] = $label;
      }
      else {
        $vars[$line] = TRUE;
      }
    }
    return $vars;
  }

  /**
   * Return an array of key => value from a given module setting.
   *
   * @param string $module
   *   The module name.
   * @param string $key
   *   The config key.
   *
   * @return array
   *   An array of ID => Label.
   *
   * @see Atools::configGet
   * @see Atools::getVarsArray
   */
  public static function getConfVarsArray($module, $key) {
    return self::getVarsArray(self::configGet($module, $key));
  }

  /**
   * Replace D7 url() function.
   *
   * Return an URL from a path (internal or external).
   *
   * Accept :
   *  - node/x
   *  - /path/to/content
   *  - http(s)://www.exemple.com
   *
   * @param string $path
   *   The path to get the URL from.
   * @param array $options
   *   (optional) An array of options. See Url::fromUri() for details.
   * @param bool $asString
   *   (optional) Return URL object or String.
   *
   * @return \Drupal\Core\Url|string
   *   URL object or string.
   */
  public static function urlFromPath($path, array $options = [], bool $asString = TRUE) {
    if ($path != '') {
      if (UrlHelper::isExternal($path)) {
        $url = Url::fromUri($path, $options);
      }
      else {
        if (strpos($path, '/') !== 0) {
          $path = '/' . $path;
        }
        $url = Url::fromUserInput($path, $options);
      }
      return $asString ? $url->toString() : $url;
    }
    return '';
  }

  /**
   * Show debug messages if module enable it and user has permission.
   *
   * Use Drupal Message or Watchdog by default.
   *
   * @param string $module
   *   The module machine_name.
   * @param array $vars
   *   The variables to show ('name' => 'content').
   * @param bool $message
   *   (optional) Use Drupal message.
   * @param string $permission
   *   (optional) User permission to access debug message.
   *
   * @return string
   *   The debug message text if $message = FALSE.
   */
  public static function debug($module, array $vars, $message = FALSE, $permission = 'debug atools') {
    if (self::configGet($module, 'debug', FALSE) && \Drupal::currentUser()->hasPermission($permission)) {
      $text = '';
      foreach ($vars as $key => $value) {
        if ($message === TRUE) {
          if (is_string($value)) {
            \Drupal::messenger()->addMessage('DEBUG: ' . $key . ' = ' . $value);
          }
          else {
            $rendered_message = Markup::create('DEBUG: ' . $key . '<pre>' . print_r($value, TRUE) . '</pre>');
            \Drupal::messenger()->addMessage($rendered_message);
          }
        }
        else {
          $text .= "DEBUG : {$key}<pre>" . print_r($value, TRUE) . '</pre>';
          \Drupal::logger($module)->debug($text);
        }
      }
      if ($text !== '') {
        return $text;
      }
    }
  }

  /**
   * Get a module setting value from a key or the config object.
   *
   * @param string $module
   *   Module name.
   * @param string $key
   *   (Optional) The config key.
   * @param mixed $default
   *   (Optional) The default value if key does not exist.
   *
   * @staticvar array $config
   *
   * @return mixed
   *   The value or the default value if key does not exist.
   */
  public static function configGet($module, $key = '', $default = FALSE) {
    $config = &drupal_static(__FUNCTION__, []);

    // Load module config.
    if (!isset($config[$module])) {
      $config[$module] = \Drupal::config($module . '.settings');
    }

    if ($key === '') {
      return $config[$module];
    }

    if (!empty($value = $config[$module]->get($key))) {
      return $value;
    }
    else {
      return $default;
    }
  }

  /**
   * Helper function from the system_theme_settings form.
   *
   * Attempts to validate normal system paths, paths relative to the public
   * files directory, or stream wrapper URIs.
   * If the given path is any of the above,
   * returns a valid path or URI that the theme system can display.
   *
   * @param string $path
   *   A path relative to the Drupal root or to the public files directory, or
   *   a stream wrapper URI.
   *
   * @return bool|string
   *   A valid path that can be displayed through the theme system, or FALSE if
   *   the path could not be validated.
   */
  public static function validatePath($path) {
    // Absolute local file paths are invalid.
    if (\Drupal::service('file_system')->realpath($path) == $path) {
      return FALSE;
    }
    // A path relative to the Drupal root or a fully qualified URI is valid.
    if (is_file($path)) {
      return $path;
    }
    // Prepend 'public://' for relative file paths within public filesystem.
    if (file_uri_scheme($path) === FALSE) {
      $path = 'public://' . $path;
    }
    if (is_file($path)) {
      return $path;
    }
    return FALSE;
  }

  /**
   * Modified version of core watchdog_exception.
   *
   * This allow full message on GuzzleHttp Exception which are truncated.
   *
   * @param string $module
   *   Module name.
   * @param Exception $exception
   *   Exception to add log from.
   */
  public static function watchdogException($module, \Exception $exception) {
    $message = $exception->getMessage();
    if ($exception->hasResponse()) {
      $message .= "<br>\n" . $exception->getResponse()->getBody()->getContents();
    }
    watchdog_exception($module, $exception, NULL, ['@message' => $message]);
  }

  /**
   * Flatten a multidimensional array preserving keys.
   *
   * Especially useful to use or save $form_state values from keys when the
   * Form array define containers (Details, Fieldsets, ...).
   *
   * @param array $array
   *   The multidimensional array.
   *
   * @return array
   *   The flat array.
   */
  public static function flattenArray(array $array) {
    return iterator_to_array(
      new \RecursiveIteratorIterator(new \RecursiveArrayIterator($array))
    );
  }

}
