<?php

namespace Drupal\atools\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Base class for implementing system configuration forms.
 *
 * With additionnal methods and default settings.
 */
abstract class AtoolsConfigForm extends ConfigFormBase {

  use FormGenerateTrait;

  /**
   * Module machine name.
   *
   * @var string
   */
  const MODULE_NAME = 'atools';

  /**
   * Activate use of cache fields (cache & cache_time).
   *
   * @var bool
   */
  const USE_CACHE = TRUE;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);

    $this->loadConf(static::MODULE_NAME, 'module');
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return [
      static::MODULE_NAME . '.settings',
    ];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return static::MODULE_NAME . '_admin_settings';
  }

  /**
   * Return a list of field keys without the module name.
   *
   * @return array
   *   Fields names.
   */
  protected function getFields() {
    if ($this->fields !== FALSE) {
      return $this->fields;
    }
    return [
      'debug',
      'cache',
      'cache_time',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldName($key) {
    return static::MODULE_NAME . '_' . $key;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($genForm = $this->getGeneratedForm()) {
      $form = array_merge($form, $genForm);
      return parent::buildForm($form, $form_state);
    }
    
    // Here is a old part, used before form generator.
    $config = $this->configGet();

    $form['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('General settings'),
    ];

    $form['general'][$this->getFieldName('debug')] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug mode'),
      '#default_value' => $config->get('debug'),
      '#description' => $this->t('Enable debug mode to show informations'),
    ];

    if (static::USE_CACHE === TRUE) {
      $form['general'][$this->getFieldName('cache')] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Activate cache'),
        '#default_value' => $config->get('cache'),
        '#description' => $this->t('Enable caching'),
      ];

      $form['general'][$this->getFieldName('cache_time')] = [
        '#type' => 'textfield',
        '#title' => $this->t('Cache expiration'),
        '#required' => FALSE,
        '#description' => $this->t('Cache expiration in second. 0 or empty is for none (do not use)'),
        '#default_value' => $config->get('cache_time'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configGet();
    $cfgNames = $this->getFields();

    foreach ($cfgNames as $name) {
      $cfgName = $this->getFieldName($name);
      $config->set($name, $form_state->getValue($cfgName));
    }
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * Get a setting value from a key or the config object.
   *
   * @param string $key
   *   (Optionnal) The field key.
   *
   * @return mixed
   *   Value or Config object if $key is empty.
   */
  protected function configGet($key = '') {
    $config = &drupal_static(__FUNCTION__, []);

    if (!isset($config[static::MODULE_NAME])) {
      $config[static::MODULE_NAME] = $this->config(static::MODULE_NAME . '.settings');
    }

    if ($key === '') {
      return $config[static::MODULE_NAME];
    }

    return $config[static::MODULE_NAME]->get($key);
  }

  /**
   * {@inheritdoc}
   */
  protected function getSchemaName() {
    return $this->getEditableConfigNames()[0];
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldDefaultValue($key) {
    return $this->configGet($key);
  }

}
