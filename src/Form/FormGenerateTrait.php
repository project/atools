<?php

namespace Drupal\atools\Form;

use Drupal\Core\Config\FileStorage;

/**
 * Provide method to generate FORM from YML configuration files.
 *
 * This trait provides a method to load form configuration YML files.
 * With those declarations, it can generate a FORM
 * and list the fields to be saved.
 */
trait FormGenerateTrait {

  /**
   * The config form loaded from file.
   *
   * @var array|bool
   */
  private $configForm = FALSE;

  /**
   * List of config fields loaded from file.
   *
   * @var array|bool
   */
  private $fields = FALSE;

  /**
   * List of default fields values from file.
   *
   * @var array|bool
   */
  private $defaults = FALSE;

  /**
   * Load the YML configurations files for an extension.
   *
   * It is depending on the name provided by :
   * - getConfigFormName : for the form structure.
   * - getConfigFieldsName : for the list of fields.
   *
   * @param string $name
   *   The name of the extension to load from.
   * @param string $type
   *   (optional) The type of extension to load from.
   */
  protected function loadConf($name, $type = 'module') {
    $config_path = drupal_get_path($type, $name) . '/config/tools';
    $config_source = new FileStorage($config_path);

    $this->configForm = $config_source->read($this->getConfigFormName());
    $this->fields = $config_source->read($this->getConfigFieldsName());

    // If the fields array is associative, default values are provided.
    if ($this->fields !== FALSE && !isset($this->fields[0])) {
      $this->defaults = $this->fields;
      $this->fields = array_keys($this->fields);
    }
  }

  /**
   * Get the generated form based on fields from file.
   *
   * @return array
   *   Result form.
   */
  protected function getGeneratedForm() {
    return ($this->configForm !== FALSE) ? $this->generateForm($this->configForm) : [];
  }

  /**
   * Generate additionnal fields on the settings form.
   *
   * It use the %NAME%.form.yml file configuration available in modules.
   *
   * @param array $conf
   *   An associative array containing the settings for the form.
   *
   * @return array
   *   The generated form.
   */
  protected function generateForm(array $conf) {
    static $renderElements = [
      'actions',
      'ajax',
      'container',
      'contextual_links',
      'contextual_links_placeholder',
      'details',
      'dropbutton',
      'field_ui_table',
      'fieldgroup',
      'fieldset',
      'form',
      'html',
      'html_tag',
      'inline_template',
      'label',
      'link',
      'inline_template',
      'label',
      'link',
      'more_link',
      'operations',
      'page',
      'page_title',
      'pager',
      'processed_text',
      'responsive_image',
      'status_messages',
      'system_compact_link',
      'text_format',
      'toolbar',
      'toolbar_item',
      'vertical_tabs',
      'view',
    ];
    $form = [];

    foreach ($conf as $key => $values) {
      $subForm = [];
      if (isset($values['_fields'])) {
        $subForm = $this->generateForm($values['_fields']);
        unset($values['_fields']);
      }
      $element = $this->generateField($values);

      if (!isset($element['#type']) || in_array($element['#type'], $renderElements)) {
        // Add the element to the form.
        $form[$key] = $element + $subForm;
      } else {
        // Add the default value.
        if ($default = $this->getFieldDefaultValue($key)) {
          $element['#default_value'] = $default;
        }
        // Add the element to the form.
        $form[$this->getFieldName($key)] = $element + $subForm;
      }
    }

    return $form;
  }

  /**
   * Generate a field for the form.
   *
   * @param array $conf
   *   The configuration for the element to generate.
   *
   * @return array
   *   The generated element.
   */
  protected function generateField(array $conf) {
    $element = [];

    foreach ($conf as $key => $value) {
      switch (TRUE) {
        case $key == 'title':
        case $key == 'description':
          $val = $this->t($value);
          break;

        case $key == 'markup':
          $val = '<div>' . $this->t($value) . '</div>';
          break;

        default:
          $val = $value;
          break;
      }
      $element['#' . $key] = $val;
    }

    return $element;
  }

  /**
   * Return the list of field keys.
   *
   * @return array
   *   Fields names.
   */
  protected function getFields() {
    return $this->fields ?: [];
  }

  /**
   * Gets the settings form config name, to load config from YML file.
   *
   * @return string
   *   The name of the fields list conf.
   */
  protected function getConfigFormName() {
    return $this->getSchemaName() . '.form';
  }

  /**
   * Gets the fields list config name, to load fields list from YML file.
   *
   * @return string
   *   The name of the fields list conf.
   */
  protected function getConfigFieldsName() {
    return $this->getSchemaName() . '.fields';
  }

  /**
   * Gets the settings form config name, to load config from YML file.
   *
   * @return string
   *   The name of the fields list conf.
   */
  abstract protected function getSchemaName();

  /**
   * Return a unique name for a form field depending on the key.
   *
   * @param string $key
   *   The field key.
   *
   * @return string
   *   Form element name.
   */
  protected function getFieldName($key) {
    return $this->getSchemaName() . '-' . $key;
  }

  /**
   * Get a field default value from a key.
   *
   * @param string $key
   *   The field key.
   *
   * @return mixed
   *   The value.
   */
  protected function getFieldDefaultValue($key) {
    return isset($this->defaults[$key]) ? $this->defaults[$key] : NULL;
  }
}
