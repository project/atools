<?php

namespace Drupal\atools\Form;

use Drupal\Atools\Atools;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Atools base class for implementing block.
 *
 * With additionnal methods and generated form.
 */
abstract class AtoolsBlockBase extends BlockBase {

  use FormGenerateTrait;

  /**
   * Module machine name.
   *
   * @var string
   */
  const MODULE_NAME = 'atools';

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $this->loadConf(static::MODULE_NAME, 'module');

    if ($genForm = $this->getGeneratedForm()) {
      $form = array_merge($form, $genForm);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $cfgNames = $this->getFields();
    $values = Atools::flattenArray($form_state->getValues());

    foreach ($cfgNames as $cfgName) {
      $name = $this->getFieldName($cfgName);

      $this->configuration[$cfgName] = $values[$name];
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldName($key) {
    return $this->getBaseId() . '_' . $key;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSchemaName() {
    return $this->getBaseId();
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldDefaultValue($key) {
    return isset($this->configuration[$key]) ? $this->configuration[$key] : NULL;
  }

}
